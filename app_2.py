"""
This is second application.
Created by: Mykola Rudyk
"""

from collections import deque

# import support code
import example_graph
import proj_2

# general imports
import urllib.request as urllib2
import random
import time
import math

# Desktop imports
import matplotlib.pyplot as plt


##########################################################
# Code for loading computer network graph

NETWORK_URL = "http://storage.googleapis.com/codeskulptor-alg/alg_rf7.txt"


def load_graph(graph_url):
    """
    Function that loads a graph given the URL
    for a text representation of the graph
    
    Returns a dictionary that models a graph
    """
    graph_file = urllib2.urlopen(graph_url)
    graph_text = graph_file.read().decode("utf8")
    graph_lines = graph_text.split('\n')
    graph_lines = graph_lines[ : -1]
    
    print ("Loaded graph with", len(graph_lines), "nodes")
    
    answer_graph = {}
    for line in graph_lines:
        neighbors = line.split(' ')
        node = int(neighbors[0])
        answer_graph[node] = set([])
        for neighbor in neighbors[1 : -1]:
            answer_graph[node].add(int(neighbor))

    return answer_graph


def create_er_graph(num_nodes, prob):
    """
    """
    er_graph = {}
    for itr_node in range (0, num_nodes):
        er_graph[itr_node] = set([])

    for itr_node in range (0, num_nodes):
        for itr_neighbour in range (0, num_nodes):
            if (itr_node != itr_neighbour) and (prob > random.random()):
                er_graph[itr_node].add(itr_neighbour)
                er_graph[itr_neighbour].add(itr_node)


    return er_graph



def random_order(graph):
    """
    Function that takes graph and return random order of nodes from it
    """
        
    return 0

# test_graph = load_graph(NETWORK_URL)

# print (create_er_graph(20,0.25))
print (1235.54/(3047*3047*2))
