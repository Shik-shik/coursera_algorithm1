"""
This is second Project.
Created by: Mykola Rudyk
"""

from collections import deque

EX_GRAPH0 = {0: set([1,2]),
             1: set([]),
             2: set([])
            }

EX_GRAPH2 = {0: set([1,5,4]), 
             1: set([2,6]),
             2: set([3,7]),
             3: set([7]),
             4: set([1]),
             5: set([2]),
             6: set([]),
             7: set([3]),
             8: set([2,3]),
             9: set([0,3,4,5,6,7])
            }
EX_GRAPH3 = {0: set([1]),
             1: set([0]),
             2: set([3]),
             3: set([2])}

GRAPH0 = {0: set([1]),
          1: set([0, 2]),
          2: set([1, 3]),
          3: set([2])}

GRAPH5 = {"dog": set(["cat"]),
          "cat": set(["dog"]),
          "monkey": set(["banana"]),
          "banana": set(["monkey", "ape"]),
          "ape": set(["banana"])}

GRAPH4 = {0: set([1, 2, 3, 4]),
          1: set([0]),
          2: set([0]),
          3: set([0]),
          4: set([0]),
          5: set([6, 7]),
          6: set([5]),
          7: set([5])}

def bfs_visited(ugraph, start_node):
    """
    Function: takes the undirected graph and start node

    Return: set of all nodes that are visited by a breadth-first search
    """
    print ("bfs_visited >>>> [function in]")
    print ("input graph: ", ugraph)
    tmp_queue = deque()
    print ("tmp_queue: ", tmp_queue)
    visited = set()
    visited.add(start_node)
    tmp_queue.append(start_node)
    print ("q: ", tmp_queue)
    while tmp_queue:
        itr = tmp_queue.pop() 
        for neighbour in ugraph[itr]: 
            print ("node itr ", ugraph[itr])
            if neighbour not in visited:
                visited.add(neighbour)
                tmp_queue.append(neighbour)
                print ("tmp_queue: ", tmp_queue)
    print ("bfs_visited >>>> output:")
    print (visited)
    print ("bfs_visited >>>> [function out]")
    return visited

def cc_visited(ugraph):
    """
    Function: takes the undirected graph

    Return: list of sets, where each set consist of all the nodes in a 
    connected component
    """
    print ("cc_visited >>>> [function in]")
    remaining_nodes = set(ugraph)
    print ("remaining_nodes: ", remaining_nodes)
    conn_comp = list()
    while remaining_nodes:
        itr_1 = remaining_nodes.pop()
        print ("node from remaining nodes:", itr_1)
        list_visited = bfs_visited(ugraph, itr_1)
        print ("list_visited: ", list_visited)
        conn_comp.append(list_visited) 
        remaining_nodes -= list_visited
        print ("not")
    return conn_comp


def largest_cc_size(ugraph):
    """
    Function: takes the undirected graph

    Return: size of the largest connected component in ugraph
    """
    print ("largest_cc_size>>>> [function in]")
    remaining_nodes = set(ugraph)
    print ("remaining_nodes: ", remaining_nodes)
    max_cc_size = 0
    while remaining_nodes:
        itr_1 = remaining_nodes.pop()
        print ("node from remaining nodes:", itr_1)
        list_visited = bfs_visited(ugraph, itr_1)
        print ("list_visited: ", list_visited)
        if len(list_visited)>max_cc_size:
            max_cc_size = len(list_visited)
        remaining_nodes -= list_visited
    print ("end largest_cc_size")
    return max_cc_size

def compute_resilience(ugraph, attack_order):
    """
    Function: takes the undirected graph, a list of nodes attack order and iterates.

    Return: list whose k+1th entry is the size of the largest connected component in graph
    """
    resilience = list([largest_cc_size(ugraph)])
    for itr_1 in attack_order:
        print ("itr_1: ",itr_1)
        for itr in ugraph:
            # copy_ugraph[itr] = ugraph[itr].copy()
            if itr_1 in ugraph[itr]:
                ugraph[itr].remove(itr_1)
                print ("copy_ugraph without one set:", ugraph)
        del ugraph[itr_1]
        print("graph before going into max:", ugraph)
        resilience.append(largest_cc_size(ugraph))
    return resilience


print (compute_resilience(GRAPH0, list([1,2])))

# print (cc_visited(GRAPH5))
# print (largest_cc_size(GRAPH5))
# print (bfs_visited(EX_GRAPH2, 0))

