# CodeSkulptor runs Python programs in your browser.
# Click the upper left button to run this simple demo.

# CodeSkulptor runs in Chrome 18+, Firefox 11+, and Safari 6+.
# Some features may work in other browsers, but do not expect
# full functionality.  It does NOT run in Internet Explorer.


message = "Welcome!"

print (message)

f = open ('IntegerArray.txt', 'r')


integer_array = []

for line in f:
    integer_array.append(int (line))


def merge_sort (array, arr_len):
    print (array)
    print (arr_len)
    if arr_len ==1:
        return 0
    else:
        merge_sort (array[:int (arr_len/2)], int(arr_len/2))
        merge_sort (array[int((arr_len - arr_len/2)):], int(arr_len - arr_len/2))
        output_array = compute_merge_inversions (array, arr_len)
        for k in range(0,arr_len):
            array[k] = output_array[k]
        return array

def compute_merge_inversions(array, arr_len):
    output_array = []
    i = 0 
    j = 0
    for k in range(0,arr_len):
        print ("k = ", k)
        if (j == int(arr_len/2)):
            output_array.append(array[i])
            i+=1
            print ("first index: ", i)
        elif (array[i] < array[j+int(arr_len/2)]):
            output_array.append(array[i])
            i+=1
            print ("first index: ", i)  
        else:
            output_array.append(array[j+int(arr_len/2)])
            j+=1
            print ("second index: ", j)
    print (output_array)
    return output_array

print (merge_sort ([5,4,2,1], 4))
    
# print (integer_array)
